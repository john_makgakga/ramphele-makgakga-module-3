import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF00BF6D);
const kSecondaryColor = Color.fromARGB(255, 39, 1, 254);
const kContentColorLightTheme = Color(0xFF1D1D35);
const kContentColorDarkTheme = Color(0xFFF5FCF9);
const kWarninngColor = Color.fromARGB(255, 243, 28, 28);
const kErrorColor = Color(0xFFF03738);
const bgColor = Color(0xFF212332);
const kDefaultPadding = 20.0;
const primaryColor = Color(0xFF2697FF);
const secondaryColor = Color(0xFF2A2D3E);
